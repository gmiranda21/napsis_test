## Instalacion Sistema

El usuario que se crea es perfil unico. Para entrar al sistema el acceso es:

Usuario o Correo Electrónico: **admin@napsis.com**

Contraseña: **12345678**

Las tablas Hospitals, Pacientes, Especialista, Tipo Consulta y Tipo de Estado se llenan con data de prueba.

El modelo de BD es el siguiente:

[ModeloBD-napsis_test.svg](https://bitbucket.org/gmiranda21/napsis_test/src/master/ModeloBD-napsis_test.svg) o [ModeloBD-napsis_test.mwb](https://bitbucket.org/gmiranda21/napsis_test/src/master/ModeloBD-napsis_test.mwb) (En MYSQL Workbench)

### Requisitos del sistema

Puede ser xampp en Windows o servidor Linux

- **Apache 2.4.48**
- **PHP 7.3+**
- **MYSQL 5.2+ o MariaDB 10.4.20**
- **Composer 2.1.3**
- **Node 14.17.3**
- **Npm 6.14.13**

### Crear la base de datos

```
CREATE DATABASE napsis_test;
```

### Crear usuario de la BD

```
CREATE USER 'napsis_test'@'localhost' IDENTIFIED BY '$$Napsis.2217$$';
GRANT ALL PRIVILEGES ON napsis_test.* TO 'napsis_test'@'localhost' WITH GRANT OPTION;
```

### Configuración del proyecto

```
composer install
```

```
npm install
```

```
php artisan optimize:clear
```


```
npm run development
```

### Creación y llenado de tablas para la base de datos

```
php artisan db:seed
```

```
composer dump-autoload
```

```
php artisan migrate:fresh --seed
```

---

