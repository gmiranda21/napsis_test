<?php

return [

    'MENU_HEADER_DASHBOARD' => 'Dashboard',
    'MENU_HEADER_001' => 'Hospital',
    'MENU_HEADER_002' => 'Paciente',
    'MENU_HEADER_003' => 'Consulta',
    'MENU_HEADER_004' => 'Especialista',
    'MENU_HEADER_005' => 'Cerrar sesión',
    'MENU_HEADER_BUSCAR' => 'Buscar',
    'TEXTO_BUSCADO_INFO' => 'Texto buscado: ',
    'TEXTO_TOP' => 'Parte superior',
    'MSG_TITLE_DASHBOARD' => 'Dashboard',

    'MSG_TITLE_HOSPITAL' => 'Hospital',
    'MSG_AGREGAR_HOSPITAL' => 'Hospital agregado exitosamente',
    'MSG_NO_AGREGAR_HOSPITAL' => 'Hospital no agregado',
    'MSG_MODIFICAR_HOSPITAL' => 'Hospital modificado exitosamente',
    'MSG_NO_MODIFICAR_HOSPITAL' => 'Hospital no modificado',
    'MSG_ELIMINAR_HOSPITAL' => 'Hospital eliminado exitosamente',
    'MSG_NO_ELIMINAR_HOSPITAL' => 'Hospital NO eliminado',

    'MSG_TITLE_PACIENTE' => 'Pacientes',
    'MSG_AGREGAR_PACIENTE' => 'Paciente agregado exitosamente',
    'MSG_NO_AGREGAR_PACIENTE' => 'Paciente no agregado',
    'MSG_MODIFICAR_PACIENTE' => 'Paciente modificado exitosamente',
    'MSG_NO_MODIFICAR_PACIENTE' => 'Paciente no modificado',
    'MSG_ELIMINAR_PACIENTE' => 'Paciente eliminado exitosamente',
    'MSG_NO_ELIMINAR_PACIENTE' => 'Paciente NO eliminado',

    'MSG_TITLE_CONSULTA' => 'Consultas',
    'MSG_AGREGAR_CONSULTA' => 'Consulta agregada exitosamente',
    'MSG_NO_AGREGAR_CONSULTA' => 'Consulta no agregada',
    'MSG_MODIFICAR_CONSULTA' => 'Consulta modificada exitosamente',
    'MSG_NO_MODIFICAR_CONSULTA' => 'Consulta no modificada',
    'MSG_ELIMINAR_CONSULTA' => 'Consulta eliminada exitosamente',
    'MSG_NO_ELIMINAR_CONSULTA' => 'Consulta NO eliminada',

    'MSG_TITLE_ESPECIALISTA' => 'Especialistas',
    'MSG_AGREGAR_ESPECIALISTA' => 'Especialista agregado exitosamente',
    'MSG_NO_AGREGAR_ESPECIALISTA' => 'Especialista no agregado',
    'MSG_MODIFICAR_ESPECIALISTA' => 'Especialista modificado exitosamente',
    'MSG_NO_MODIFICAR_ESPECIALISTA' => 'Especialista no modificado',
    'MSG_ELIMINAR_ESPECIALISTA' => 'Especialista eliminado exitosamente',
    'MSG_NO_ELIMINAR_ESPECIALISTA' => 'Especialista NO eliminado',

    'MSG_NO_HAY_REGISTRO' => 'No hay registros',
    'MSG_ERROR_REGISTRO' => 'Error de registro',
    'BTN_VOLVER' => 'Volver',
    'BTN_GRABAR' => 'Grabar',
    'BTN_MODIFICAR' => 'Modificar',
    'MSG_TOOLTIP_INGRESAR' => 'Crear',
    'MSG_TOOLTIP_MODIFICAR' => 'Editar',
    'MSG_TOOLTIP_ELIMINAR' => 'Eliminar',
    'MSG_TITLE_CONFIRM' => 'Confirmar',
    'MSG_DESCRIPCION_CONFIRM' => 'Desea eliminar el registro'
];
