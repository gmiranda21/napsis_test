<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'title_idioma' => 'idioma',
    'title_idioma_1' => 'Español',
    'title_idioma_2' => 'Ingles',
    'title_iniciar_sesion' => 'iniciar sesión',
    'title_registrarse' => 'Registrarse',
    'title_salir' => 'cerrar sesión',
    'title_direccion_email' => 'Dirección de correo electrónico',
    'title_contrasenna' => 'Contraseña',
    'title_confirmar_contrasenna' => 'Confirmar contraseña',
    'title_recuerdame' => 'recuerdame',
    'title_olvido_contrasenna' => 'Olvidaste tu contraseña?',
    'title_restablecer_contrasenna' => 'Restablecer la contraseña',
    'title_enviar_enlace_contrasenna' => 'Enviar enlace de contraseña',
    'title_verificar_direccion_email' => 'Verifique su dirección de correo electrónico',
    'title_enviado_nuevo_enlace' => 'Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.',
    'title_revise_email_enlace' => 'Antes de continuar, revise su correo electrónico para obtener un enlace de verificación.',
    'title_no_recibiste_correo' => 'Si no recibiste el correo electrónico haga clic aquí para solicitar otro',
    'title_haga_clic_solicitar' => 'haga clic aquí para solicitar otro',
    'title_form_nombre' => 'Nombre',
    'title_form_email' => 'Dirección de correo electrónico',
    'title_form_contrasenna' => 'Contraseña',
    'title_form_confirmar_contrasenna' => 'Confirmar contraseña',
    'title_form_perfil' => 'Perfil',


];
