<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'title_idioma' => 'language',
    'title_idioma_1' => 'Spanish',
    'title_idioma_2' => 'English',
    'title_iniciar_sesion' => 'login',
    'title_registrarse' => 'register',
    'title_salir' => 'logout',
    'title_direccion_email' => 'E-Mail Address',
    'title_contrasenna' => 'Password',
    'title_confirmar_contrasenna' => 'Confirm Password',
    'title_recuerdame' => 'Remember Me',
    'title_olvido_contrasenna' => 'Forgot Your Password?',
    'title_restablecer_contrasenna' => 'Reset Password',
    'title_enviar_enlace_contrasenna' => 'Send Password Reset Link',
    'title_verificar_direccion_email' => 'Verify Your Email Address',
    'title_enviado_nuevo_enlace' => 'A fresh verification link has been sent to your email address.',
    'title_revise_email_enlace' => 'Before proceeding, please check your email for a verification link.',
    'title_no_recibiste_correo' => 'If you did not receive the email',
    'title_haga_clic_solicitar' => 'Click here to request another',
    'title_form_nombre' => 'Name',
    'title_form_email' => 'E-Mail Address',
    'title_form_contrasenna' => 'Password',
    'title_form_confirmar_contrasenna' => 'Confirm Password',
    'title_form_perfil' => 'Profile',

];
