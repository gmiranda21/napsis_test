<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container" style="padding-top: 0 !important;">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ env('APP_URL') }}/images/Logo-napsis-nuevo-horizontal-color-1-3.png">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" id="menu-dashboard">
                    <a class="nav-link" href="{{ env('APP_URL') }}/dashboard">{{ trans('messages.MENU_HEADER_DASHBOARD') }}</a>
                </li>
                <li class="nav-item" id="menu-hospital">
                    <a class="nav-link" href="{{ env('APP_URL') }}/getListHospital">{{ trans('messages.MENU_HEADER_001') }}</a>
                </li>
                <li class="nav-item" id="menu-paciente">
                    <a class="nav-link" href="{{ env('APP_URL') }}/getListPaciente">{{ trans('messages.MENU_HEADER_002') }}</a>
                </li>
                <!-- <li class="nav-item" id="menu-consulta">
                    <a class="nav-link" href="{{ env('APP_URL') }}/getListConsulta">{{ trans('messages.MENU_HEADER_003') }}</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> {{ trans('messages.MENU_HEADER_005') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </a>
                </li>
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @endguest
            </ul>
        </div>
    </div>
</nav>
