<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ env('APP_NAME') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="{{ env('APP_URL') }}/js/app.js"></script>

        <link href="{{ env('APP_URL') }}/css/app.css" rel="stylesheet">

        <script>
            function soloNumeros(e) {
                var key = window.event ? e.which : e.keyCode;
                if (key < 48 || key > 59) {
                    e.preventDefault();
                }
            }
        </script>

</head>

<body>
    @extends('header')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <h2>{{ $title }}</h2>

            <div class="col-lg-12">

                <form action="{{ env('APP_URL') }}/{{ $action }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" id="staticId" name="staticId" value="{{ $id }}">
                    <div class="form-group row">
                        <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" maxlength="191" class="form-control" id="staticNombre" name="staticNombre" value="{{ $nombre }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="edad" class="col-sm-2 col-form-label">Edad</label>
                        <div class="col-sm-1">
                            <input type="text" maxlength="3" class="form-control" id="staticEdad" name="staticEdad" onKeyPress="return soloNumeros(event)" value="{{ $edad }}">
                        </div>
                    </div>
                    <div class="form-group row" id="ninno_pesoestatura">
                        <label for="pesoEstatura" class="col-sm-2 col-form-label">Peso-Estatura</label>
                        <div class="col-sm-3">
                        <select class="form-control text-capitalize" id="staticPesoEstatura" name="staticPesoEstatura">
                                <option value="" selected>-- Seleccione --</option>
                                @for($i=1; $i < 5; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" id="joven_fumador">
                        <label for="fumador" class="col-sm-2 col-form-label">Fumador</label>
                        <div class="col-sm-1">
                            <input type="checkbox" id="staticFumador" name="staticFumador" value="1">
                        </div>
                        <div class="col-sm-8" id="AnnoFumadorId" style="display:none;">
                            <div class="row">
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" placeholder="Años Fumando" id="staticAnnoFumador" name="staticAnnoFumador" onKeyPress="return soloNumeros(event)" value="{{ $annoFumador }}">
                                </div>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="dependencia" class="col-form-label">Dependencia</label>
                                        </div>
                                        <div class="col-lg-2">
                                            <input type="checkbox" id="staticDependencia" name="staticDependencia" value="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="anciano_dieta">
                        <label for="tienedieta" class="col-sm-2 col-form-label">Tiene Dieta</label>
                        <div class="col-sm-1">
                            <input type="checkbox" id="staticTieneDieta" name="staticTieneDieta" value="1">
                        </div>
                    </div>

                    <div class="text-right">
                        <a class="btn btn-primary" href="{{ env('APP_URL') }}/getListPaciente" role="button">{{ trans('messages.BTN_VOLVER') }}</a> <input class="btn btn-primary" type="submit" value="{{ $boton }}">
                    </div>

                    <p></p>

                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @extends('footer')

</body>
</html>
