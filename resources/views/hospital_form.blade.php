<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ env('APP_NAME') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="{{ env('APP_URL') }}/js/app.js"></script>

        <link href="{{ env('APP_URL') }}/css/app.css" rel="stylesheet">

</head>

<body>
    @extends('header')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <h2>{{ $title }}</h2>

            <div class="col-lg-12">

                <form action="{{ env('APP_URL') }}/{{ $action }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" id="staticId" name="staticId" value="{{ $id }}">
                    <div class="form-group row">
                        <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" maxlength="191" class="form-control" id="staticNombre" name="staticNombre" value="{{ $nombre }}">
                        </div>
                    </div>

                    <div class="text-right">
                        <a class="btn btn-primary" href="{{ env('APP_URL') }}/getListHospital" role="button">{{ trans('messages.BTN_VOLVER') }}</a> <input class="btn btn-primary" type="submit" value="{{ $boton }}">
                    </div>

                    <p></p>

                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @extends('footer')

</body>
</html>
