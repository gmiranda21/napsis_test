@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ trans('auth.title_verificar_direccion_email') }}</div>
                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ trans('auth.title_enviado_nuevo_enlace') }}
                    </div>
                    @endif
                    {{ trans('auth.title_revise_email_enlace') }}
                    {{ trans('auth.title_no_recibiste_correo') }}, <a href="{{ route('verification.resend') }}">{{ trans('auth.title_haga_clic_solicitar') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
