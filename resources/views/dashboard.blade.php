<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ env('APP_NAME') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ env('APP_URL') }}/js/app.js"></script>
    <link href="{{ env('APP_URL') }}/css/app.css" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function() {
            $('#staticIdHospital').on('change', function() {
                document.forms['selectIdHospital'].submit();
            });
        });
        </script>
</head>

<body>
    @extends('header')
    <!-- Page Content -->
    <div class="container">
        <h2>Dashboard</h2>
        <div class="row mb-3">            
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                      Hospital
                    </div>
                    <div class="card-body">
                        @if($hospital->count() > 0)
                      <h5 class="card-title">Seleccione el Hospital</h5>
                      <p class="card-text">Debe seleccionar el hospital para poder seguir con el proceso de ingreso de paciente.</p>                      
                      <form action="/dashboard" method="get" name="selectIdHospital" id="selectIdHospital">
                      <select class="form-control text-capitalize" id="staticIdHospital" name="staticIdHospital">
                            <option value="" selected>-- Seleccione --</option>
                            @if($hospital->count() > 0)
                            @foreach ($hospital as $value)
                            <?php $selected_update = '' ?>
                            @if ($intHospital == $value->id_hospital)
                            <?php $selected_update = 'selected' ?>
                            @endif
                            <option value="{{ $value->id_hospital }}" @if(old('staticIdHospital')==$value->id_hospital){{'selected'}}@else{{$selected_update}}@endif>{{ $value->nombre }}</option>
                            @endforeach
                            @endif
                        </select>
                    </form>
                    @else
                    <a href="/createHospital" class="btn btn-primary btn-lg" tabindex="-1" role="button" aria-disabled="true">Crear Hospital</a>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        {{-- LISTAR PACIENTES --}}
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                      Pacientes
                    </div>
                    <div class="card-body">

                        {{-- MAYOR RIESGO --}}
                        <div class="row mb-3">
                            <h5 class="card-title ml-3">Mayor Riesgo</h5>
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Edad</th>
                                            <th scope="col">Historia Clinica</th>
                                            <th scope="col">Riesgo</th>
                                        </tr>
                                    </thead>
                                    @if($pacienteMayorRiesgo->count() > 0)
                                    @foreach ($pacienteMayorRiesgo as $value)
                                    <tbody>
                                        <tr>
                                            <th scope="row" class="text-right">{{$value->id_paciente}}</th>
                                            <td>{{$value->nombre}}</td>
                                            <td class="text-right">{{$value->edad}}</td>
                                            <td class="text-right">{{$value->noHistoriaClinica}}</td>
                                            <td class="text-right">{{$value->riesgo}}</td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                    @else
                                    <tbody>
                                        <tr>
                                            <td colspan="5" style="text-align: center">{{ trans('messages.MSG_NO_HAY_REGISTRO') }}</td>
                                        </tr>
                                    </tbody>
                                    @endif
                                </table>
                                {{ $pacienteMayorRiesgo->appends(request()->query())->links() }}
                            </div>                            
                        </div>

                        {{-- ATENDER PACIENTE --}}
                        <div class="row mb-3">
                            <div class="col-lg-4">
                                <div class="card text-black mb-3" style="max-width: 18rem;">
                                    <div class="card-header">Atender Paciente</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <button type="button" class="btn btn-primary" id="atenderPaciente">
                                                Acci&oacute;n
                                                </button>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="spinner-border" role="status" style="display: none;">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="col-lg-4">
                                <div class="card text-black mb-3" style="max-width: 18rem;">
                                    <div class="card-header">Paciente m&aacute;s anciano</div>
                                    <div class="card-body">
                                    <h5 class="card-title">{{$pacienteMasAncianoSalaEsperaNombre}} / {{$pacienteMasAncianoSalaEsperaEdad}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        {{-- FUMADORES URGENTE --}}
                        <div class="row mb-3">
                            <h5 class="card-title ml-3">Fumadores Urgente</h5>
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Edad</th>
                                            <th scope="col">A&ntilde;os Fumando</th>
                                            <th scope="col">Historia Clinica</th>
                                        </tr>
                                    </thead>
                                    @if($pacientesMasUrgenteFumadores->count() > 0)
                                    @foreach ($pacientesMasUrgenteFumadores as $value)
                                    <tbody>
                                        <tr>
                                            <th scope="row" class="text-right">{{$value->id_paciente}}</th>
                                            <td>{{$value->nombre}}</td>
                                            <td class="text-right">{{$value->edad}}</td>
                                            <td class="text-right">{{$value->anno_fumador}}</td>
                                            <td class="text-right">{{$value->noHistoriaClinica}}</td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                    @else
                                    <tbody>
                                        <tr>
                                            <td colspan="5" style="text-align: center">{{ trans('messages.MSG_NO_HAY_REGISTRO') }}</td>
                                        </tr>
                                    </tbody>
                                    @endif
                                </table>
                                {{ $pacientesMasUrgenteFumadores->appends(request()->query())->links() }}
                            </div>                            
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                      Consultas
                    </div>
                    <div class="card-body">

                        {{-- LIBERAR CONSULTAS --}}
                        <div class="row mb-3">
                            <div class="col-lg-4">
                                <div class="card text-black mb-3" style="max-width: 18rem;">
                                    <div class="card-header">Liberar Consultas</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <button type="button" class="btn btn-primary" id="liberarConsulta">
                                                Acci&oacute;n
                                                </button>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="spinner-border" role="status" style="display: none;">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- CONSULTA MAS PACIENTES ATENTIDOS --}}
                        <div class="row mb-3">
                            <div class="col-lg-4">
                                <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                                    <div class="card-header">Consulta Mas Pacientes Atentidos</div>
                                    <div class="card-body">
                                    <h5 class="card-title"></h5>
                                    <p class="card-text">
                                        @if(!empty($consultaMasPacientesAtentidos))
                                        <ul class="list-group bg-primary">
                                            <li class="list-group-item bg-primary"><strong>Id:</strong> {{$consultaMasPacientesAtentidos->id_consulta}}</li>
                                            <li class="list-group-item bg-primary"><strong>Cantidad Pacientes:</strong> {{$consultaMasPacientesAtentidos->cantidad_pacientes}}</li>
                                            <li class="list-group-item bg-primary"><strong>Hospital:</strong> {{$consultaMasPacientesAtentidos->nombreHospital}}</li>
                                            <li class="list-group-item bg-primary"><strong>Consulta:</strong> {{$consultaMasPacientesAtentidos->nombreConsulta}}</li>
                                            <li class="list-group-item bg-primary"><strong>Estado:</strong> {{$consultaMasPacientesAtentidos->nombreEstado}}</li>
                                        </ul>
                                        @endif
                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    @extends('footer')
</body>

</html>
