<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{env('APP_NAME')}}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="{{ env('APP_URL') }}/js/app.js"></script>

        <link href="{{ env('APP_URL') }}/css/app.css" rel="stylesheet">
    </head>

    <body>

        @if ($type == 'productos')
            @extends('productos')
        @elseif ($type == 'categorias')
            @extends('categorias')
        @else
            @extends('home')
        @endif

    </body>
</html>
