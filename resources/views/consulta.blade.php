<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ env('APP_NAME') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ env('APP_URL') }}/js/app.js"></script>
    <link href="{{ env('APP_URL') }}/css/app.css" rel="stylesheet">
    <script type="text/javascript">
        setTimeout(function() {
            $(".alert-success").fadeOut('slow');
            $(".alert-danger").fadeOut('slow');
        }, 2500);
    </script>
</head>

<body>
    @extends('header')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <h2>{{ $title }}</h2>
            <div class="col-lg-12">
                @if (session('status'))
                <div class="alert alert-success text-center" role="alert">
                    <strong>{{ session('status') }}</strong>
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger text-center" role="alert">
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif
                <div class="spinner-border" role="status" style="display: none;">
                    <span class="sr-only">Loading...</span>
                </div>
                <div style="text-align: right;">
                    <a href="{{ env('APP_URL') }}/createConsulta" data-toggle="tooltip" data-placement="top" title="{{ trans('messages.MSG_TOOLTIP_INGRESAR') }}"><i class="fas fa-plus-square"></i></a>
                </div>
                <div class="table-responsive-lg">
                    <table class="table table-striped table-sm table-hover table-bordered col-lg-12">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Cantidad Pacientes</th>
                                <th scope="col">Nombre Especialista</th>
                                <th scope="col">Hospital</th>
                                <th scope="col">Tipo Consulta</th>
                                <th scope="col">Tipo Estado</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        @if($cantidad > 0)
                        @foreach ($returnedrows as $value)
                        <tbody>
                            <tr id="{{ $value->id_consulta }}">
                                <th scope="row" class="text-right">{{ $value->id_consulta }}</th>
                                <td class="text-right">{{ $value->cantPacientes }}</td>
                                <td>{{ $value->nombreEspecialista }}</td>
                                <td>{{ $value->NombreHospital }}</td>
                                <td>{{ $value->NombreConsulta }}</td>
                                <td>{{ $value->NombreEstado }}</td>
                                <td class="text-center"><a href="getConsulta/{{ $value->id_consulta }}" data-toggle="tooltip" data-placement="top" title="{{ trans('messages.MSG_TOOLTIP_MODIFICAR') }}"><i class="far fa-edit"></i></a></td>
                                <td class="text-center"><a href="#" class="btn-delete" data-id="{{ $value->id_consulta }}" data-url="deleteConsulta" data-title-confirm="{{ trans('messages.MSG_TITLE_CONFIRM') }}" data-text-confirm="{{ trans('messages.MSG_DESCRIPCION_CONFIRM') }}" data-toggle="tooltip" data-placement="top" title="{{ trans('messages.MSG_TOOLTIP_ELIMINAR') }}"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>
                        </tbody>
                        @endforeach
                        @else
                        <tbody>
                            <tr>
                                <td colspan="8" style="text-align: center">{{ trans('messages.MSG_NO_HAY_REGISTRO') }}</td>
                            </tr>
                        </tbody>
                        @endif
                    </table>
                </div>
                {{ $returnedrows->appends(request()->query())->links() }}
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    @extends('footer')
</body>

</html>
