<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{{ env('APP_NAME') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="{{ env('APP_URL') }}/js/app.js"></script>

        <link href="{{ env('APP_URL') }}/css/app.css" rel="stylesheet">

        <script>
            function soloNumeros(e) {
                var key = window.event ? e.which : e.keyCode;
                if (key < 48 || key > 59) {
                    e.preventDefault();
                }
            }
        </script>

</head>

<body>
    @extends('header')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <h2>{{ $title }}</h2>

            <div class="col-lg-12">

                <form action="{{ env('APP_URL') }}/{{ $action }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" id="staticId" name="staticId" value="{{ $id }}">
                    <div class="form-group row">
                        <label for="cantPacientes" class="col-sm-2 col-form-label">Cantidad Pacientes</label>
                        <div class="col-sm-1">
                            <input type="text" maxlength="4" class="form-control" id="staticCantPacientes" name="staticCantPacientes" onKeyPress="return soloNumeros(event)" value="{{ $cantPacientes }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nombreEspecialista" class="col-sm-2 col-form-label">Nombre Especialista</label>
                        <div class="col-sm-10">
                            <input type="text" maxlength="191" class="form-control" id="staticNombreEspecialista" name="staticNombreEspecialista" value="{{ $nombreEspecialista }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="idHospital" class="col-sm-2 col-form-label">Hospital</label>
                        <div class="col-sm-4">
                        <select class="form-control text-capitalize" id="staticIdHospital" name="staticIdHospital">
                                <option value="" selected>-- Seleccione --</option>
                                @if($hospital->count() > 0)
                                @foreach ($hospital as $value)
                                <?php $selected_update = '' ?>
                                @if ($intHospital == $value->id_hospital)
                                <?php $selected_update = 'selected' ?>
                                @endif
                                <option value="{{ $value->id_hospital }}" @if(old('staticIdHospital')==$value->id_hospital){{'selected'}}@else{{$selected_update}}@endif>{{ $value->nombre }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tipoConsulta" class="col-sm-2 col-form-label">Tipo Consulta</label>
                        <div class="col-sm-3">
                        <select class="form-control text-capitalize" id="staticIdTipoConsulta" name="staticIdTipoConsulta">
                                <option value="" selected>-- Seleccione --</option>
                                @if($tipoConsulta->count() > 0)
                                @foreach ($tipoConsulta as $value)
                                <?php $selected_update = '' ?>
                                @if ($intTipoConsulta == $value->id_tipoconsulta)
                                <?php $selected_update = 'selected' ?>
                                @endif
                                <option value="{{ $value->id_tipoconsulta }}" @if(old('staticIdTipoConsulta')==$value->id_tipoconsulta){{'selected'}}@else{{$selected_update}}@endif>{{ $value->nombre }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tipoEstado" class="col-sm-2 col-form-label">Estado</label>
                        <div class="col-sm-3">
                        <select class="form-control text-capitalize" id="staticIdEstado" name="staticIdEstado">
                            <option value="" selected>-- Seleccione --</option>
                                @if($tipoEstado->count() > 0)
                                @foreach ($tipoEstado as $value)
                                <?php $selected_update = '' ?>
                                @if ($intEstado == $value->id_tipoestado)
                                <?php $selected_update = 'selected' ?>
                                @endif                          
                                <option value="{{ $value->id_tipoestado }}" @if(old('staticIdEstado')==$value->id_tipoestado){{'selected'}}@else{{$selected_update}}@endif>{{ $value->nombre }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="text-right">
                        <a class="btn btn-primary" href="{{ env('APP_URL') }}/getListConsulta" role="button">{{ trans('messages.BTN_VOLVER') }}</a> <input class="btn btn-primary" type="submit" value="{{ $boton }}">
                    </div>

                    <p></p>

                </form>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @extends('footer')

</body>
</html>
