/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jquery-napsis.js');

import 'jquery-ui/ui/widgets/datepicker.js';
import 'jquery-ui/themes/base/theme.css';
import 'jquery-ui/themes/base/datepicker.css';
import 'jquery-ui/ui/widgets/sortable.js';
import 'jquery-ui/ui/widgets/autocomplete.js';
import 'jquery-confirm/dist/jquery-confirm.min.js';

import 'tablesorter/dist/js/jquery.tablesorter.js';
import 'tablesorter/dist/js/jquery.tablesorter.widgets.js';
import 'tablesorter/dist/css/theme.default.min.css';
import 'jquery-confirm/dist/jquery-confirm.min.css';

window._ = require('lodash');
window.Popper = require('popper.js').default;

