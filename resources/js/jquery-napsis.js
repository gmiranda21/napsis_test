$(document).ready(function() {

    $('#ninno_pesoestatura, #joven_fumador, #anciano_dieta').find('*').attr('disabled', true);

    $('.link-header').find('*').attr('disabled', true);

    $('[data-toggle="tooltip"]').tooltip();

    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $("#top").css('display', 'block');
        } else {
            $("#top").css('display', 'none');
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    $("#top").click(function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });

    var activeurl = window.location.pathname;
    $(".navbar-nav li").removeClass('active');
    $(".navbar-nav li").each(function() {
        if (activeurl == "/getListPaciente" || activeurl == "/createPaciente" || location.pathname.indexOf('getPaciente') == 1) {
            $("#menu-paciente").addClass("active");
        } else if (activeurl == "/getListHospital" || activeurl == "/createHospital" || location.pathname.indexOf('getHospital') == 1) {
            $("#menu-hospital").addClass("active");
        } else if (activeurl == "/getListConsulta" || activeurl == "/createConsulta" || location.pathname.indexOf('getConsulta') == 1) {
            $("#menu-consulta").addClass("active");
        } else if (activeurl == "/getListEspecialista" || activeurl == "/createEspecialista" || location.pathname.indexOf('getEspecialista') == 1) {
            $("#menu-especialista").addClass("active");
        } else{
            $("#menu-dashboard").addClass("active");
        }
    });

    //INICIO VALIDACIONES FORMULARIOS
    function caracteresCorreoValido(email, div) {
        var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

        if (caract.test(email) == false) {
            $('#button-save').prop('disabled', true);
            $(div).css('display', 'block');
            return false;
        } else {
            $(div).css('display', 'none');
            return true;
        }
    }

    $('input[type=email]').blur(function() {
        caracteresCorreoValido($(this).val(), '#error-email')
    });

    //FIN  VALIDACIONES FORMULARIOS

    $(".datapicker").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function(dateText, inst) {
            $(inst).val(dateText); // Write the value in the input
        }
    });

    $("#staticFumador").click(function() {
        if($(this).is(':checked')){
            $('#AnnoFumadorId').show();
        }else{
            $('#AnnoFumadorId').hide();
            $('#staticAnnoFumador').val('');
        }
    });

    $("#staticEdad").change(function() {
        console.log($(this).val());
        if($(this).val() >= 1 && $(this).val() <= 15){
            $('#ninno_pesoestatura').find('*').attr('disabled', false);
            $('#staticFumador').prop('checked', false);
            $('#staticTieneDieta').prop('checked', false);
            $('#AnnoFumadorId').hide();
            $('#staticAnnoFumador').val('');
            $('#joven_fumador').find('*').attr('disabled', true);
            $('#anciano_dieta').find('*').attr('disabled', true);            
        }else if($(this).val() >= 16 && $(this).val() <= 40){
            $('#ninno_pesoestatura').find('*').attr('disabled', true);
            $('#joven_fumador').find('*').attr('disabled', false);
            $('#anciano_dieta').find('*').attr('disabled', true);
            $('#staticTieneDieta').prop('checked', false);
            $("#staticPesoEstatura option")
                    .removeAttr("selected")
                    .filter("[value='']")
                    .attr("selected", "selected");
        }else if($(this).val() >= 41){
            $('#ninno_pesoestatura').find('*').attr('disabled', true);
            $('#staticFumador').prop('checked', false);
            $('#AnnoFumadorId').hide();
            $('#staticAnnoFumador').val('');
            $('#joven_fumador').find('*').attr('disabled', true);
            $('#anciano_dieta').find('*').attr('disabled', false);
            $("#staticPesoEstatura option")
                    .removeAttr("selected")
                    .filter("[value='']")
                    .attr("selected", "selected");
        }
    });

    $(".btn-delete").click(function() {
        var id = $(this).data("id");
        var url = $(this).data("url");
        var titleConfirm = $(this).data("title-confirm");
        var textConfirm = $(this).data("text-confirm");
        $.confirm({
            title: titleConfirm,
            content: "\u00BF" + textConfirm + "?",
            buttons: {
                confirm: {
                    btnClass: "btn-blue",
                    action: function() {
                        $.ajax({
                            type: "DELETE",
                            url: url + '/' + id,
                            dataType: "JSON",
                            data: {
                                id: id,
                                _method: "delete",
                                _token: $('meta[name="csrf-token"]').attr(
                                    "content"
                                )
                            },
                            beforeSend: function() {
                                $(".spinner-border").show();
                            },
                            success: function(result) {
                                $(".spinner-border").hide();
                                if (result.success) {
                                    $.alert({
                                        title: "",
                                        content:result.message,
                                        buttons: {
                                            ok: {
                                                btnClass: "btn-green",
                                                action: function() {
                                                    $("#" + id).animate({
                                                        height: "toggle"
                                                    },
                                                    "fast",
                                                    function() {
                                                        $(this).remove();
                                                    });
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    $.alert(result.message);
                                }
                            },
                            error: function() {
                                $(".spinner-border").hide();
                                $.alert("Operation error!");
                            }
                        });
                    }
                },
                cancel: {
                    btnClass: "btn-red any-other-class",
                    action: function() {}
                }
            }
        });
    });

    $("#atenderPaciente").click(function() {
        $.ajax({
            type: "GET",
            url: "atenderPaciente",
            dataType: "JSON",
            data: {
                _token: $('meta[name="csrf-token"]').attr("content")
            },
            beforeSend: function() {
                $(".spinner-border").show();
            },
            success: function(result) {
                $(".spinner-border").hide();
                if (result.success) {
                    $.alert({
                        title: "",
                        content:result.message,
                        buttons: {
                            ok: {
                                btnClass: "btn-green",
                                action: function() {
                                }
                            }
                        }
                    });
                } else {
                    $.alert(result.message);
                }
            },
            error: function() {
                $(".spinner-border").hide();
                $.alert("Operation error!");
            }
        });
    });
    
});
