<?php

use Illuminate\Database\Seeder;
use App\Paciente;
use Carbon\Carbon;

class PacienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //GENERACION PACIENTOS NIÑOS DE 1-5
        for($i=1; $i <= 10; $i++){
            $paciente = new Paciente();
            $paciente->nombre = 'Paciente Niño 0'.$i;
            $paciente->edad = rand(1,15);
            $paciente->noHistoriaClinica = mt_Rand(10000000, 99999999);
            $paciente->peso_estatura = rand(1,4);
            $paciente->prioridad = rand(0,10);
            $paciente->riesgo = rand(1,6);
            $paciente->created_at = Carbon::now()->toDateTimeString();
            $paciente->updated_at = Carbon::now()->toDateTimeString();
            $paciente->save();
        }

        //GENERACION PACIENTOS JOVENES DE 16-40
        for($i=1; $i <= 10; $i++){
            $paciente = new Paciente();
            $paciente->nombre = 'Paciente Joven 0'.$i;
            $paciente->edad = rand(16,40);
            $paciente->noHistoriaClinica = mt_Rand(10000000, 99999999);
            $randFumador = rand(0,1);
            if($randFumador){
                $paciente->fumador = 1;
                $paciente->anno_fumador = rand(2,5);
                $paciente->dependencia = rand(0,1);
            }else{
                $paciente->fumador = 0;
            }
            $paciente->prioridad = rand(0,10);
            $paciente->riesgo = rand(1,6);
            $paciente->created_at = Carbon::now()->toDateTimeString();
            $paciente->updated_at = Carbon::now()->toDateTimeString();
            $paciente->save();
        }

        //GENERACION PACIENTOS ANCIANOS DE 16-40
        for($i=1; $i <= 10; $i++){
            $paciente = new Paciente();
            $paciente->nombre = 'Paciente Anciano 0'.$i;
            $paciente->edad = rand(41,100);
            $paciente->noHistoriaClinica = mt_Rand(10000000, 99999999);
            $paciente->tiene_dieta = rand(0,1);
            $paciente->prioridad = rand(0,10);
            $paciente->riesgo = rand(1,6);
            $paciente->created_at = Carbon::now()->toDateTimeString();
            $paciente->updated_at = Carbon::now()->toDateTimeString();
            $paciente->save();
        }        
        

     }
}
