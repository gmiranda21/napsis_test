<?php

use Illuminate\Database\Seeder;
use App\TipoConsulta;
use Carbon\Carbon;

class TipoConsultaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoConsulta = new TipoConsulta();
        $tipoConsulta->nombre = 'Pediatría';
        $tipoConsulta->created_at = Carbon::now()->toDateTimeString();
        $tipoConsulta->updated_at = Carbon::now()->toDateTimeString();
        $tipoConsulta->save();

        $tipoConsulta = new TipoConsulta();
        $tipoConsulta->nombre = 'Urgencia';
        $tipoConsulta->created_at = Carbon::now()->toDateTimeString();
        $tipoConsulta->updated_at = Carbon::now()->toDateTimeString();
        $tipoConsulta->save();

        $tipoConsulta = new TipoConsulta();
        $tipoConsulta->nombre = 'CGI';
        $tipoConsulta->created_at = Carbon::now()->toDateTimeString();
        $tipoConsulta->updated_at = Carbon::now()->toDateTimeString();
        $tipoConsulta->save();

       
     }
}
