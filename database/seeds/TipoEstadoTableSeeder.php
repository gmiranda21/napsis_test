<?php

use Illuminate\Database\Seeder;
use App\TipoEstado;
use Carbon\Carbon;

class TipoEstadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoEstado = new TipoEstado();
        $tipoEstado->nombre = 'Ocupada';
        $tipoEstado->created_at = Carbon::now()->toDateTimeString();
        $tipoEstado->updated_at = Carbon::now()->toDateTimeString();
        $tipoEstado->save();

        $tipoEstado = new TipoEstado();
        $tipoEstado->nombre = 'Sala de Espera';
        $tipoEstado->created_at = Carbon::now()->toDateTimeString();
        $tipoEstado->updated_at = Carbon::now()->toDateTimeString();
        $tipoEstado->save();

        $tipoEstado = new TipoEstado();
        $tipoEstado->nombre = 'Desocupada';
        $tipoEstado->created_at = Carbon::now()->toDateTimeString();
        $tipoEstado->updated_at = Carbon::now()->toDateTimeString();
        $tipoEstado->save();

       
     }
}
