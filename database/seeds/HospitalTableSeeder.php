<?php

use Illuminate\Database\Seeder;
use App\Hospital;
use Carbon\Carbon;

class HospitalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //GENERACION DE HOSPITALES
        for($i=1; $i <= 5; $i++){
            $paciente = new Hospital();
            $paciente->nombre = 'Hospital Test 0'.$i;
            $paciente->created_at = Carbon::now()->toDateTimeString();
            $paciente->updated_at = Carbon::now()->toDateTimeString();
            $paciente->save();
        }
        

     }
}
