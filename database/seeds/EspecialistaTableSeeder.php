<?php

use Illuminate\Database\Seeder;
use App\TipoConsulta;
use App\Especialista;
use Carbon\Carbon;

class EspecialistaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_consulta_pediatria = TipoConsulta::where('nombre', 'Pediatría')->first();
        $especialista = new Especialista();
        $especialista->nombre = 'Doc. A Pedriatría';
        $especialista->tipo_consulta_idtipo_consulta = $tipo_consulta_pediatria->idtipo_consulta;
        $especialista->created_at = Carbon::now()->toDateTimeString();
        $especialista->updated_at = Carbon::now()->toDateTimeString();
        $especialista->save();

        $tipo_consulta_urgencia = TipoConsulta::where('nombre', 'Urgencia')->first();
        $especialista = new Especialista();
        $especialista->nombre = 'Doc. A Urgencia';
        $especialista->tipo_consulta_idtipo_consulta = $tipo_consulta_urgencia->idtipo_consulta;
        $especialista->created_at = Carbon::now()->toDateTimeString();
        $especialista->updated_at = Carbon::now()->toDateTimeString();
        $especialista->save();

        $tipo_consulta_cgi = TipoConsulta::where('nombre', 'CGI')->first();
        $especialista = new Especialista();
        $especialista->nombre = 'Doc. A CGI';
        $especialista->tipo_consulta_idtipo_consulta = $tipo_consulta_cgi->idtipo_consulta;
        $especialista->created_at = Carbon::now()->toDateTimeString();
        $especialista->updated_at = Carbon::now()->toDateTimeString();
        $especialista->save();

     }
}
