<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // La creación de datos de roles debe ejecutarse primero
	    $this->call(RoleTableSeeder::class);
	    
        // Los usuarios necesitarán los roles previamente generados
	    $this->call(UserTableSeeder::class);

        // GENERA LOS TIPOS DE CONSULTAS
        $this->call(TipoConsultaTableSeeder::class);

        // GENERA LOS TIPOS DE ESTADO
        $this->call(TipoEstadoTableSeeder::class);

        // GENERA LOS ESPECIALISTA CON ASOCIADO CON EL TIPO DE CONSULTA: Pediatría, Urgencia o CGI
        $this->call(EspecialistaTableSeeder::class);

        // GENERA PACIENTES NIÑOS, JOVENES Y ANCIANOS. 5 DE CADA TIPO
        $this->call(PacienteTableSeeder::class);

        // GENERA 5 HOSPITALES
        $this->call(HospitalTableSeeder::class);
        
    }
}
