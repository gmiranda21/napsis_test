<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id_paciente');
            $table->string('nombre')->nullable();
            $table->integer('edad')->nullable();
            $table->integer('noHistoriaClinica');
            $table->boolean('fumador')->nullable()->default(0);
            $table->boolean('dependencia')->nullable()->default(0);
            $table->integer('anno_fumador')->nullable();
            $table->boolean('tiene_dieta')->nullable()->default(0);
            $table->integer('peso_estatura')->nullable()->default(0);
            $table->decimal('prioridad', 5, 2)->nullable()->default(0);
            $table->decimal('riesgo', 5, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
