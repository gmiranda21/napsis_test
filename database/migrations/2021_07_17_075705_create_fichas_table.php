<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fichas', function (Blueprint $table) {
            $table->bigIncrements('id_ficha');
            //$table->string('estado', 45)->nullable();
            $table->bigInteger('tipo_estado_idtipo_estado')->unsigned()->nullable();
            $table->foreign('tipo_estado_idtipo_estado')->references('idtipo_estado')->on('tipo_estados');
            $table->bigInteger('paciente_id_paciente')->unsigned()->nullable();
            $table->foreign('paciente_id_paciente')->references('id_paciente')->on('pacientes');
            $table->bigInteger('consulta_id_consulta')->unsigned()->nullable();
            $table->foreign('consulta_id_consulta')->references('id_consulta')->on('consultas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fichas');
    }
}
