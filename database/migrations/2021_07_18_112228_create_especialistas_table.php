<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialistas', function (Blueprint $table) {
            $table->bigIncrements('id_especialista');
            $table->string('nombre', 191)->nullable();
            $table->bigInteger('tipo_consulta_idtipo_consulta')->unsigned()->nullable();
            $table->foreign('tipo_consulta_idtipo_consulta')->references('idtipo_consulta')->on('tipo_consultas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialistas');
    }
}
