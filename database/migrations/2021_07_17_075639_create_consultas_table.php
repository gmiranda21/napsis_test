<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas', function (Blueprint $table) {
            $table->bigIncrements('id_consulta');
            $table->integer('cantidad_pacientes')->nullable()->default(0);
            $table->string('nombre_especialista', 45)->nullable();

            $table->bigInteger('hospital_id_hospital')->unsigned()->nullable();
            $table->foreign('hospital_id_hospital')->references('id_hospital')->on('hospitals');

            $table->bigInteger('tipo_consulta_idtipo_consulta')->unsigned()->nullable();
            $table->foreign('tipo_consulta_idtipo_consulta')->references('idtipo_consulta')->on('tipo_consultas');

            $table->bigInteger('tipo_estado_idtipo_estado')->unsigned()->nullable();
            $table->foreign('tipo_estado_idtipo_estado')->references('idtipo_estado')->on('tipo_estados');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas');
    }
}
