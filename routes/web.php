<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'DashboardController@index');
Route::get('/home', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index');


//HOSPITAL
Route::get('/getListHospital', 'HospitalController@index');
Route::get('/createHospital', 'HospitalController@create');
Route::post('/setHospital', 'HospitalController@store');
Route::post('/updateHospital', 'HospitalController@update');
Route::get('/getHospital/{id}', 'HospitalController@edit');
Route::delete('/deleteHospital/{id}', 'HospitalController@destroy');

//PACIENTE
Route::get('/getListPaciente', 'PacienteController@index');
Route::get('/createPaciente', 'PacienteController@create');
Route::post('/setPaciente', 'PacienteController@store');
Route::post('/updatePaciente', 'PacienteController@update');
Route::get('/getPaciente/{id}', 'PacienteController@edit');
Route::delete('/deletePaciente/{id}', 'PacienteController@destroy');
Route::get('/atenderPaciente', 'PacienteController@atenderPaciente');

//CONSULTA
Route::get('/getListConsulta', 'ConsultaController@index');
Route::get('/createConsulta', 'ConsultaController@create');
Route::post('/setConsulta', 'ConsultaController@store');
Route::post('/updateConsulta', 'ConsultaController@update');
Route::get('/getConsulta/{id}', 'ConsultaController@edit');
Route::delete('/deleteConsulta/{id}', 'ConsultaController@destroy');

