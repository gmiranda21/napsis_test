<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    //
    protected $table = 'fichas';

    public $primaryKey = 'id_ficha';

    public function scopeIdPaciente($query, $idPaciente)
    {
        return $query->where('paciente_id_paciente', $idPaciente);
    }
}
