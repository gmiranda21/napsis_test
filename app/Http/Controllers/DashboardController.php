<?php

/**
 *
 * @author Gonzalo Miranda <gmiranda.marambio21@gmail.com>
 *
 */

namespace App\Http\Controllers;

use App\Hospital;
use App\Paciente;
use App\Ficha;
use App\Consulta;
use App\TipoConsulta;
use App\TipoEstado;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {

            if(isset($request->staticIdHospital)){
                $nombreHospital = Hospital::find($request->staticIdHospital);
                session(['sesion_int_hospital' => $request->staticIdHospital]);
                session(['sesion_name_hospital' => $nombreHospital->nombre]);
            }
            
            $hospital = Hospital::orderBy('nombre', 'ASC')->get();

            //PACIENTES MAYOR RIESGO
            $pacientesMayorRiesgo = Paciente::select('id_paciente', 'nombre', 'edad', 'noHistoriaClinica', 'riesgo')
                                    ->orderBy('riesgo', 'DESC')
                                    ->paginate(env('NUM_PAGE_ROW_DASHBOARD'));

            /* PACIENTE MAS ANCIANO */
            $pacienteMasAncianoSalaEspera = Consulta::join('fichas', 'consultas.id_consulta', '=', 'fichas.consulta_id_consulta')
                                    ->join('pacientes', 'fichas.paciente_id_paciente', '=', 'pacientes.id_paciente')
                                    ->select('pacientes.nombre', 'pacientes.edad')
                                    ->where('consultas.tipo_estado_idtipo_estado', 2) // ESTADO SALA DE ESPERA
                                    ->whereRaw('pacientes.edad = (SELECT MAX( edad ) FROM pacientes)')
                                    ->first();

            /* PACIENTE MAS URGENTE FUMADORES */
            $pacientesMasUrgenteFumadores = Paciente::select('id_paciente', 'nombre', 'edad', 'anno_fumador', 'noHistoriaClinica', 'riesgo')
                                    ->where('fumador', 1) // SI SON FUMADORES
                                    ->where('dependencia', 1) // SI SON FUMADORES
                                    ->orderBy('anno_fumador', 'DESC')
                                    ->paginate(env('NUM_PAGE_ROW_DASHBOARD'));

            /* CONSULTA CON MAS PACIENTES ATENTIDOS */
            $consultaMasPacientesAtentidos = Consulta::join('tipo_consultas', 'consultas.tipo_consulta_idtipo_consulta', '=', 'tipo_consultas.idtipo_consulta')
                                    ->join('tipo_estados', 'consultas.tipo_estado_idtipo_estado', '=', 'tipo_estados.idtipo_estado')
                                    ->join('hospitals', 'consultas.hospital_id_hospital', '=', 'hospitals.id_hospital')
                                    ->select('consultas.id_consulta', 'consultas.cantidad_pacientes', 'hospitals.nombre as nombreHospital', 'tipo_consultas.nombre as nombreConsulta', 'tipo_estados.nombre as nombreEstado')
                                    ->whereRaw('consultas.cantidad_pacientes = (SELECT MAX( cantidad_pacientes ) FROM consultas)')
                                    ->first();

            /* CONSULTAS */
            //$consultasUrgenciaList = Consulta::select('*')->where('');
        
            return view('dashboard', compact('hospital'))
                ->with('title', trans('messages.MSG_TITLE_DASHBOARD'))
                ->with('intHospital', session('sesion_int_hospital'))
                ->with('nombreHospital', session('sesion_name_hospital'))
                ->with('pacienteMayorRiesgo', $pacientesMayorRiesgo)
                ->with('pacienteMasAncianoSalaEsperaNombre', (!empty($pacienteMasAncianoSalaEspera->nombre) ? $pacienteMasAncianoSalaEspera->nombre : ''))
                ->with('pacienteMasAncianoSalaEsperaEdad', (!empty($pacienteMasAncianoSalaEspera->edad) ? $pacienteMasAncianoSalaEspera->edad : ''))
                ->with('pacientesMasUrgenteFumadores', $pacientesMasUrgenteFumadores)
                ->with('consultaMasPacientesAtentidos', (!empty($consultaMasPacientesAtentidos) ? $consultaMasPacientesAtentidos : ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListClientes')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListClientes')->with('error', $e->getMessage());
        }
    }

    /**
     * Muestra el formulario para crear un nuevo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form
        
    }

    /**
     * Almacena un recurso recién creado.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

    }

    /**
     * Obtiene el registro especifico por el $id
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Muestra el formulario para editar el recurso especificado.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        

    }

    /**
     * Actualiza el registro especifo por el $id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        
    }

    /**
     * Eliminar el registro especifico por el $id
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //
        
    }
}
