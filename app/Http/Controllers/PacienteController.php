<?php

/**
 *
 * @author Gonzalo Miranda <gmiranda.marambio21@gmail.com>
 *
 */

namespace App\Http\Controllers;

use App\Paciente;
use App\Hospital;
use App\Ficha;
use App\Consulta;
use App\Especialista;
use App\TipoEstado;
use App\TipoConsulta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class PacienteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {

            $returnedrows = Paciente::orderBy('prioridad', 'DESC')
                                    ->paginate(env('NUM_PAGE_ROW'));
            return view('paciente', compact('returnedrows'))
                ->with('cantidad', $returnedrows->count())
                ->with('title', trans('messages.MSG_TITLE_PACIENTE'));

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListPaciente')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListPaciente')->with('error', $e->getMessage());
        }
    }

    /**
     * Muestra el formulario para crear un nuevo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form
        try {

            return view('paciente_form', [
                'title' => trans('messages.MSG_TITLE_PACIENTE'),
                'boton' => trans('messages.BTN_GRABAR'),
                'action' => 'setPaciente',
                'readonly' => '',
                'id' => '',
                'nombre' => '',
                'edad' => '',
                'fumador' => '',
                'annoFumador' => '',
                'tieneDieta' => '',
                'noHistoriaClinica' => '',
                'pesoEstatura' => ''
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListPaciente')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListPaciente')->with('error', $e->getMessage());
        }

    }

    /**
     * Almacena un recurso recién creado.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($request, &$rowIdPaciente) {

                $pacientePrioridad = new Paciente();
                $pacienteRiesgo = new Paciente();

                $intEdad = (int) $request->staticEdad;
                $intPesoEstatura = isset($request->staticPesoEstatura) ? $request->staticPesoEstatura : 0;
                $boolFumador = isset($request->staticFumador) ? $request->staticFumador : 0;
                $intAnnoFumador = isset($request->staticAnnoFumador) ? $request->staticAnnoFumador : 0;
                $boolDependencia = isset($request->staticDependencia) ? $request->staticDependencia : 0;
                $boolTieneDieta = isset($request->staticTieneDieta) ? $request->staticTieneDieta : 0;

                $paciente = new Paciente();
                $paciente->nombre = trim($request->staticNombre);
                $paciente->edad = $intEdad;
                $paciente->noHistoriaClinica = mt_Rand(10000000, 99999999);
                $paciente->fumador = $boolFumador;
                $paciente->anno_fumador = $intAnnoFumador;
                $paciente->dependencia = $boolDependencia;
                $paciente->tiene_dieta = $boolTieneDieta;
                $paciente->peso_estatura = $intPesoEstatura;
                $intPrioridad = $pacientePrioridad->prioridad($intEdad, $intPesoEstatura, $boolFumador, $intAnnoFumador, $boolTieneDieta);
                $paciente->prioridad = $intPrioridad;
                $paciente->riesgo = $pacienteRiesgo->riesgo($intEdad, $intPrioridad);
                $paciente->created_at = Carbon::now()->toDateTimeString();
                $paciente->updated_at = Carbon::now()->toDateTimeString();
                $paciente->save();
                $rowIdPaciente = $paciente->id_paciente;

            });

            if(is_null($transaction)){
                DB::commit();
                return redirect('getListPaciente')->with('status', trans('messages.MSG_AGREGAR_PACIENTE'));
            }else{
                DB::rollBack();
                return redirect('getListPaciente')->with('error', trans('messages.MSG_NO_AGREGAR_PACIENTE'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect('getListPaciente')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect('getListPaciente')->with('error', $e->getMessage());
        }

    }

    /**
     * Atender Paciente
     *
     * @return \Illuminate\Http\Response
     */
    public function atenderPaciente(Request $request)
    {
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($request) {

                $pacientes = Paciente::all();
                if($pacientes->count() > 0){
                    
                    //MAYOR PRIORIDAD
                    $pacientesPrioridad = Paciente::orderBy('prioridad', 'DESC')
                                    ->get();
                    
                    //REVISAR SI EXISTE CONSULTA ESTADO DESOCUPADA
                    //TRAE EL ID DEL ESTADO DESOCUPADA
                    $idEstadoDesocupada = TipoEstado::idEstadoDesocupada()->first();
                    //TRAE LA CONSULTA SI ESTA DISPONIBLE CON ESTADO DESOCUPADO
                    $returnEstadoDesocupada = Consulta::estado($idEstadoDesocupada->idtipo_estado)->first();

                    if(!empty($returnEstadoDesocupada)){
                        
                    }else{
                        
                        foreach($pacientesPrioridad as $value){
                            
                            //PRIORIDAD MAYOR A 4 Y CONSULTA DE URGENCIA
                            if($value->prioridad > 4){

                                $returtIdUrgencia = Consulta::IdConsultaUrgencia()->first();
                                if(!empty($returtIdUrgencia->id_consulta)){
                                    $insertConsulta = Consulta::find($returtIdUrgencia->id_consulta);
                                }else{
                                    $insertConsulta = new Consulta();
                                }
                                $insertConsulta->cantidad_pacientes = $insertConsulta->cantidad_pacientes + 1;
                                $nombreEspecialista = Especialista::nameEspecialista(2)->first();
                                $insertConsulta->nombre_especialista = $nombreEspecialista->nombre;
                                $insertConsulta->hospital_id_hospital = session('sesion_int_hospital');
                                $insertConsulta->tipo_consulta_idtipo_consulta = 2;
                                $insertConsulta->tipo_estado_idtipo_estado = 3;
                                if(!empty($returtIdUrgencia->id_consulta)){
                                    $insertConsulta->updated_at = Carbon::now()->toDateTimeString();
                                }else{
                                    $insertConsulta->created_at = Carbon::now()->toDateTimeString();
                                    $insertConsulta->updated_at = Carbon::now()->toDateTimeString();
                                }
                                $insertConsulta->save();
                                if(!empty($returtIdUrgencia->id_consulta)){
                                    $rowIdConsulta = $returtIdUrgencia->id_consulta;
                                }else{
                                    $rowIdConsulta = $insertConsulta->id_consulta;
                                }
                                
                                //INSERTA O MODIFICA FICHA DEL PACIENTE
                                if(!empty($rowIdConsulta)){
                                    $existeFichaPaciente = Ficha::where('paciente_id_paciente', $value->id_paciente)->first();
                                    if(!empty($existeFichaPaciente)){
                                        $fichaPaciente = Ficha::find($existeFichaPaciente->id_ficha);
                                        $fichaPaciente->tipo_estado_idtipo_estado = 3;
                                        $fichaPaciente->consulta_id_consulta = $rowIdConsulta;
                                        $fichaPaciente->updated_at = Carbon::now()->toDateTimeString();
                                        $fichaPaciente->save();
                                    }else{
                                        $fichaPaciente = new Ficha();
                                        $fichaPaciente->paciente_id_paciente = $value->id_paciente;
                                        $fichaPaciente->tipo_estado_idtipo_estado = 3;
                                        $fichaPaciente->consulta_id_consulta = $rowIdConsulta;
                                        $fichaPaciente->created_at = Carbon::now()->toDateTimeString();
                                        $fichaPaciente->updated_at = Carbon::now()->toDateTimeString();
                                        $fichaPaciente->save();
                                    }
                                    
                                }
                                
                            }else{
                                
                                //CONSULTA DE PEDIATRIA Y PRIORIDAD MENOR 4
                                if($value->edad >= 1 && $value->edad <= 15 && $value->prioridad <= 4){

                                    $returtIdUrgencia = Consulta::IdConsultaPediatria()->first();
                                    if(!empty($returtIdUrgencia->id_consulta)){
                                        $insertConsulta = Consulta::find($returtIdUrgencia->id_consulta);
                                    }else{
                                        $insertConsulta = new Consulta();
                                    }
                                    $insertConsulta->cantidad_pacientes = $insertConsulta->cantidad_pacientes + 1;
                                    $nombreEspecialista = Especialista::nameEspecialista(1)->first();
                                    $insertConsulta->nombre_especialista = $nombreEspecialista->nombre;
                                    $insertConsulta->hospital_id_hospital = session('sesion_int_hospital');
                                    $insertConsulta->tipo_consulta_idtipo_consulta = 1;
                                    $insertConsulta->tipo_estado_idtipo_estado = 3;
                                    if(!empty($returtIdUrgencia->id_consulta)){
                                        $insertConsulta->updated_at = Carbon::now()->toDateTimeString();
                                    }else{
                                        $insertConsulta->created_at = Carbon::now()->toDateTimeString();
                                        $insertConsulta->updated_at = Carbon::now()->toDateTimeString();
                                    }
                                    $insertConsulta->save();
                                    if(!empty($returtIdUrgencia->id_consulta)){
                                        $rowIdConsulta = $returtIdUrgencia->id_consulta;
                                    }else{
                                        $rowIdConsulta = $insertConsulta->id_consulta;
                                    }
                                    
                                    //INSERTA O MODIFICA FICHA DEL PACIENTE
                                    if(!empty($rowIdConsulta)){
                                        $existeFichaPaciente = Ficha::where('paciente_id_paciente', $value->id_paciente)->first();
                                        if(!empty($existeFichaPaciente)){
                                            $fichaPaciente = Ficha::find($existeFichaPaciente->id_ficha);
                                            $fichaPaciente->tipo_estado_idtipo_estado = 3;
                                            $fichaPaciente->consulta_id_consulta = $rowIdConsulta;
                                            $fichaPaciente->updated_at = Carbon::now()->toDateTimeString();
                                            $fichaPaciente->save();
                                        }else{
                                            $fichaPaciente = new Ficha();
                                            $fichaPaciente->paciente_id_paciente = $value->id_paciente;
                                            $fichaPaciente->tipo_estado_idtipo_estado = 3;
                                            $fichaPaciente->consulta_id_consulta = $rowIdConsulta;
                                            $fichaPaciente->created_at = Carbon::now()->toDateTimeString();
                                            $fichaPaciente->updated_at = Carbon::now()->toDateTimeString();
                                            $fichaPaciente->save();
                                        }
                                        
                                    }                                    
                                }elseif($value->edad >=16){

                                    $returtIdUrgencia = Consulta::IdConsultaCGI()->first();
                                    if(!empty($returtIdUrgencia->id_consulta)){
                                        $insertConsulta = Consulta::find($returtIdUrgencia->id_consulta);
                                    }else{
                                        $insertConsulta = new Consulta();
                                    }
                                    $insertConsulta->cantidad_pacientes = $insertConsulta->cantidad_pacientes + 1;
                                    $nombreEspecialista = Especialista::nameEspecialista(3)->first();
                                    $insertConsulta->nombre_especialista = $nombreEspecialista->nombre;
                                    $insertConsulta->hospital_id_hospital = session('sesion_int_hospital');
                                    $insertConsulta->tipo_consulta_idtipo_consulta = 3;
                                    $insertConsulta->tipo_estado_idtipo_estado = 3;
                                    if(!empty($returtIdUrgencia->id_consulta)){
                                        $insertConsulta->updated_at = Carbon::now()->toDateTimeString();
                                    }else{
                                        $insertConsulta->created_at = Carbon::now()->toDateTimeString();
                                        $insertConsulta->updated_at = Carbon::now()->toDateTimeString();
                                    }
                                    $insertConsulta->save();
                                    if(!empty($returtIdUrgencia->id_consulta)){
                                        $rowIdConsulta = $returtIdUrgencia->id_consulta;
                                    }else{
                                        $rowIdConsulta = $insertConsulta->id_consulta;
                                    }
                                    
                                    //INSERTA O MODIFICA FICHA DEL PACIENTE
                                    if(!empty($rowIdConsulta)){
                                        $existeFichaPaciente = Ficha::where('paciente_id_paciente', $value->id_paciente)->first();
                                        if(!empty($existeFichaPaciente)){
                                            $fichaPaciente = Ficha::find($existeFichaPaciente->id_ficha);
                                            $fichaPaciente->tipo_estado_idtipo_estado = 3;
                                            $fichaPaciente->consulta_id_consulta = $rowIdConsulta;
                                            $fichaPaciente->updated_at = Carbon::now()->toDateTimeString();
                                            $fichaPaciente->save();
                                        }else{
                                            $fichaPaciente = new Ficha();
                                            $fichaPaciente->paciente_id_paciente = $value->id_paciente;
                                            $fichaPaciente->tipo_estado_idtipo_estado = 2;
                                            $fichaPaciente->consulta_id_consulta = $rowIdConsulta;
                                            $fichaPaciente->created_at = Carbon::now()->toDateTimeString();
                                            $fichaPaciente->updated_at = Carbon::now()->toDateTimeString();
                                            $fichaPaciente->save();
                                        }
                                    }         
                                }


                            }

                            
                        }
                    }

                }

            });

            if(is_null($transaction)){
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => 'Pacientes atentidos'
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'message' => 'Pacientes no atentidos'
                ]);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->errorInfo
            ]);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    /**
     * Obtiene el registro especifico por el $id
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Muestra el formulario para editar el recurso especificado.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {

            if(!empty($id)){

                $hospital = Hospital::all();

                $row  = Paciente::find($id);
                if(!empty($row->id_paciente)){
                    return view('paciente_form', [
                        'title' => trans('messages.MSG_TITLE_PACIENTE'),
                        'action' => 'updatePaciente',
                        'boton' => trans('messages.BTN_MODIFICAR'),
                        'id' => (int)$row->id_paciente,
                        'nombre' => $row->nombre,
                        'edad' => $row->edad,
                        'fumador' => '',
                        'annoFumador' => '',
                        'dependencia' => '',
                        'tieneDieta' => '',
                        'noHistoriaClinica' => $row->noHistoriaClinica
                        ])->with('hospital', $hospital);
                }

            }else{
                return redirect('getListPaciente')->with('error', trans('messages.MSG_ERROR_REGISTRO'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListPaciente')->with('error', $e->errorInfo);
        } catch (Exception $e) {
            return redirect('getListPaciente')->with('error', $e->getMessage());
        }

    }

    /**
     * Actualiza el registro especifo por el $id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::beginTransaction();
        try {

            if(!empty($request->staticId)){

                $transaction = DB::transaction(function () use ($request) {

                    $intEdad = (int) $request->staticEdad;
                    $intPesoEstatura = isset($request->staticPesoEstatura) ? $request->staticPesoEstatura : 0;
                    $boolFumador = isset($request->staticFumador) ? $request->staticFumador : 0;
                    $intAnnoFumador = isset($request->staticAnnoFumador) ? $request->staticAnnoFumador : 0;
                    $boolDependencia = isset($request->staticDependencia) ? $request->staticDependencia : 0;
                    $boolTieneDieta = isset($request->staticTieneDieta) ? $request->staticTieneDieta : 0;

                    $paciente = Paciente::find($request->staticId);
                    $paciente->nombre = trim($request->staticNombre);
                    $paciente->edad = $intEdad;
                    $paciente->noHistoriaClinica = mt_Rand(10000000, 99999999);
                    $paciente->fumador = $boolFumador;
                    $paciente->anno_fumador = $intAnnoFumador;
                    $paciente->dependencia = $boolDependencia;
                    $paciente->tiene_dieta = $boolTieneDieta;
                    $paciente->peso_estatura = $intPesoEstatura;
                    $intPrioridad = $pacientePrioridad->prioridad($intEdad, $intPesoEstatura, $boolFumador, $intAnnoFumador, $boolTieneDieta);
                    $paciente->prioridad = $intPrioridad;
                    $paciente->riesgo = $pacienteRiesgo->riesgo($intEdad, $intPrioridad);
                    $paciente->updated_at = Carbon::now()->toDateTimeString();
                    $paciente->save();
                    
                });

                if(is_null($transaction)){
                    DB::commit();
                    return redirect('getListPaciente')->with('status', trans('messages.MSG_MODIFICAR_PACIENTE'));
                }else{
                    DB::rollBack();
                    return redirect('getListPaciente')->with('error', trans('messages.MSG_NO_MODIFICAR_PACIENTE'));
                }

            }else{
                DB::rollBack();
                return redirect('getListPaciente')->with('error', trans('messages.MSG_ERROR_REGISTRO'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect('getListPaciente')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect('getListPaciente')->with('error', $e->getMessage());

        }
    }

    /**
     * Eliminar el registro especifico por el $id
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($id) {
                $idFicha = DB::table('fichas')->where('paciente_id_paciente', '=', $id)->first();
                
                if(!empty($idFicha->id_ficha)){
                    $ficha = Ficha::find($idFicha->id_ficha);
                    $ficha->delete();

                    $paciente = Paciente::find($id);
                    $paciente->delete();
                }
            });

            if(is_null($transaction)){
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => trans('messages.MSG_ELIMINAR_PACIENTE')
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'message' => trans('messages.MSG_NO_ELIMINAR_PACIENTE')
                ]);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->errorInfo
            ]);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }
}
