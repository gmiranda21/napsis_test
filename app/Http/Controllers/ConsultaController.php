<?php

/**
 *
 * @author Gonzalo Miranda <gmiranda.marambio21@gmail.com>
 *
 */

namespace App\Http\Controllers;

use App\Consulta;
use App\TipoConsulta;
use App\TipoEstado;
use App\Hospital;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class ConsultaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {

            $returnedrows = Consulta::join('Tipo_Consulta', 'Consulta.tipoConsulta', '=', 'Tipo_Consulta.id_tipoconsulta')
                        ->join('Tipo_Estado', 'Consulta.estado', '=', 'Tipo_Estado.id_tipoestado')
                        ->join('Hospital', 'Consulta.idHospital', '=', 'Hospital.id_hospital')
                        ->select('Consulta.*', 'Tipo_Consulta.nombre as NombreConsulta', 'Tipo_Estado.nombre as NombreEstado', 'Hospital.nombre as NombreHospital')
                        ->orderBy('Consulta.estado', 'ASC')
                        ->paginate(env('NUM_PAGE_ROW'));

            /* $returnedrows = Consulta::orderBy('estado', 'ASC')
                                    ->paginate(env('NUM_PAGE_ROW')); */
            return view('consulta', compact('returnedrows'))
                ->with('cantidad', $returnedrows->count())
                ->with('title', trans('messages.MSG_TITLE_CONSULTA'));

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListConsulta')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListConsulta')->with('error', $e->getMessage());
        }
    }

    /**
     * Muestra el formulario para crear un nuevo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form
        try {

            $tipoConsulta = TipoConsulta::all();
            $tipoEstado = TipoEstado::all();
            $hospital = Hospital::all();
            
            return view('consulta_form', [
                'title' => trans('messages.MSG_TITLE_CONSULTA'),
                'boton' => trans('messages.BTN_GRABAR'),
                'action' => 'setConsulta',
                'readonly' => '',
                'id' => '',
                'cantPacientes' => '',
                'nombreEspecialista' => '',
                'intTipoConsulta' => '',
                'intEstado' => '',
                'intHospital' => ''
            ])->with('tipoConsulta', $tipoConsulta)->with('tipoEstado', $tipoEstado)->with('hospital', $hospital);

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListConsulta')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListConsulta')->with('error', $e->getMessage());
        }

    }

    /**
     * Almacena un recurso recién creado.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($request, &$rowId) {

                $row = new Consulta();
                $row->cantPacientes = trim($request->staticCantPacientes);
                $row->nombreEspecialista = trim($request->staticNombreEspecialista);
                $row->tipoConsulta = $request->staticIdTipoConsulta;
                $row->estado = $request->staticIdEstado;
                $row->idHospital = $request->staticIdHospital;
                $row->created_at = Carbon::now()->toDateTimeString();
                $row->updated_at = Carbon::now()->toDateTimeString();
                $row->save();
                $rowId = $row->id_consulta;

            });

            if(is_null($transaction)){
                DB::commit();
                return redirect('getListConsulta')->with('status', trans('messages.MSG_AGREGAR_CONSULTA'));
            }else{
                DB::rollBack();
                return redirect('getListConsulta')->with('error', trans('messages.MSG_NO_AGREGAR_CONSULTA'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect('getListConsulta')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect('getListConsulta')->with('error', $e->getMessage());
        }

    }

    /**
     * Obtiene el registro especifico por el $id
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Muestra el formulario para editar el recurso especificado.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {

            if(!empty($id)){

                $row  = Consulta::find($id);

                $tipoConsulta = TipoConsulta::all();
                $tipoEstado = TipoEstado::all();
                $hospital = Hospital::all();

                if(!empty($row->id_consulta)){
                    return view('consulta_form', [
                        'title' => trans('messages.MSG_TITLE_CONSULTA'),
                        'action' => 'updateConsulta',
                        'boton' => trans('messages.BTN_MODIFICAR'),
                        'id' => (int)$row->id_consulta,
                        'cantPacientes' => $row->cantPacientes,
                        'nombreEspecialista' => $row->nombreEspecialista,
                        'intTipoConsulta' => $row->tipoConsulta,
                        'intEstado' => $row->estado,
                        'intHospital' => $row->idHospital
                        ])->with('tipoConsulta', $tipoConsulta)->with('tipoEstado', $tipoEstado)->with('hospital', $hospital);
                }

            }else{
                return redirect('getListConsulta')->with('error', trans('messages.MSG_ERROR_REGISTRO'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListConsulta')->with('error', $e->errorInfo);
        } catch (Exception $e) {
            return redirect('getListConsulta')->with('error', $e->getMessage());
        }

    }

    /**
     * Actualiza el registro especifo por el $id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::beginTransaction();
        try {

            if(!empty($request->staticId)){

                $transaction = DB::transaction(function () use ($request) {

                    $row = Consulta::find($request->staticId);
                    $row->cantPacientes = trim($request->staticCantPacientes);
                    $row->nombreEspecialista = trim($request->staticNombreEspecialista);
                    $row->tipoConsulta = $request->staticIdTipoConsulta;
                    $row->estado = $request->staticIdEstado;
                    $row->idHospital = $request->staticIdHospital;
                    $row->updated_at = Carbon::now()->toDateTimeString();
                    $row->save();

                });

                if(is_null($transaction)){
                    DB::commit();
                    return redirect('getListConsulta')->with('status', trans('messages.MSG_MODIFICAR_CONSULTA'));
                }else{
                    DB::rollBack();
                    return redirect('getListConsulta')->with('error', trans('messages.MSG_NO_MODIFICAR_CONSULTA'));
                }

            }else{
                DB::rollBack();
                return redirect('getListConsulta')->with('error', trans('messages.MSG_ERROR_REGISTRO'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect('getListConsulta')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect('getListConsulta')->with('error', $e->getMessage());

        }
    }

    /**
     * Eliminar el registro especifico por el $id
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($id) {
                $consulta = Consulta::find($id);
                $consulta->delete();
            });

            if(is_null($transaction)){
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => trans('messages.MSG_ELIMINAR_CONSULTA')
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'message' => trans('messages.MSG_NO_ELIMINAR_CONSULTA')
                ]);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->errorInfo
            ]);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }
}
