<?php

/**
 *
 * @author Gonzalo Miranda <gmiranda.marambio21@gmail.com>
 *
 */

namespace App\Http\Controllers;

use App\Hospital;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class HospitalController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {

            $returnedrows = Hospital::orderBy('nombre', 'ASC')
                                    ->paginate(env('NUM_PAGE_ROW'));
            return view('hospital', compact('returnedrows'))
                ->with('cantidad', $returnedrows->count())
                ->with('title', trans('messages.MSG_TITLE_HOSPITAL'));

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListHospital')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListHospital')->with('error', $e->getMessage());
        }
    }

    /**
     * Muestra el formulario para crear un nuevo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form
        try {

            return view('hospital_form', [
                'title' => trans('messages.MSG_TITLE_HOSPITAL'),
                'boton' => trans('messages.BTN_GRABAR'),
                'action' => 'setHospital',
                'readonly' => '',
                'id' => '',
                'nombre' => ''
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListHospital')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            return redirect('getListHospital')->with('error', $e->getMessage());
        }

    }

    /**
     * Almacena un recurso recién creado.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($request, &$rowId) {

                $row = new Hospital();
                $row->nombre = $request->staticNombre;
                $row->created_at = Carbon::now()->toDateTimeString();
                $row->updated_at = Carbon::now()->toDateTimeString();
                $row->save();
                $rowId = $row->id_hospital;

            });

            if(is_null($transaction)){
                DB::commit();
                return redirect('getListHospital')->with('status', trans('messages.MSG_AGREGAR_HOSPITAL'));
            }else{
                DB::rollBack();
                return redirect('getListHospital')->with('error', trans('messages.MSG_NO_AGREGAR_HOSPITAL'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect('getListHospital')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect('getListHospital')->with('error', $e->getMessage());
        }

    }

    /**
     * Obtiene el registro especifico por el $id
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Muestra el formulario para editar el recurso especificado.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {

            if(!empty($id)){

                $row  = Hospital::find($id);
                if(!empty($row->id_hospital)){
                    return view('hospital_form', [
                        'title' => trans('messages.MSG_TITLE_HOSPITAL'),
                        'action' => 'updateHospital',
                        'boton' => trans('messages.BTN_MODIFICAR'),
                        'id' => (int)$row->id_hospital,
                        'nombre' => $row->nombre
                        ]);
                }

            }else{
                return redirect('getListHospital')->with('error', trans('messages.MSG_ERROR_REGISTRO'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect('getListHospital')->with('error', $e->errorInfo);
        } catch (Exception $e) {
            return redirect('getListHospital')->with('error', $e->getMessage());
        }

    }

    /**
     * Actualiza el registro especifo por el $id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::beginTransaction();
        try {

            if(!empty($request->staticId)){

                $transaction = DB::transaction(function () use ($request) {

                    $row = Hospital::find($request->staticId);
                    $row->nombre = trim($request->staticNombre);
                    $row->updated_at = Carbon::now()->toDateTimeString();
                    $row->save();

                });

                if(is_null($transaction)){
                    DB::commit();
                    return redirect('getListHospital')->with('status', trans('messages.MSG_MODIFICAR_HOSPITAL'));
                }else{
                    DB::rollBack();
                    return redirect('getListHospital')->with('error', trans('messages.MSG_NO_MODIFICAR_HOSPITAL'));
                }

            }else{
                DB::rollBack();
                return redirect('getListHospital')->with('error', trans('messages.MSG_ERROR_REGISTRO'));
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect('getListHospital')->with('error', $e->errorInfo);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect('getListHospital')->with('error', $e->getMessage());

        }
    }

    /**
     * Eliminar el registro especifico por el $id
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //
        DB::beginTransaction();
        try {

            $transaction = DB::transaction(function () use ($id) {
                $hospital = Hospital::find($id);
                $hospital->delete();
            });

            if(is_null($transaction)){
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => trans('messages.MSG_ELIMINAR_HOSPITAL')
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'message' => trans('messages.MSG_NO_ELIMINAR_HOSPITAL')
                ]);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->errorInfo
            ]);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }
}
