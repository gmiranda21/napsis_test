<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Consulta;

class Paciente extends Model
{
    //
    protected $table = 'pacientes';

    public $primaryKey = 'id_paciente';

    public $globalPrioridad = 0;
    public $globalRiesgo = 0;
    public $globalEdad = 0;

    public function prioridad($edad, $pesoEstatura, $fumador, $annoFumador, $dieta)
    {
        if($edad >= 1 && $edad <= 15){
            if($edad >= 1 && $edad <= 5){
                $this->globalPrioridad = $pesoEstatura + 3;
            }
            if($edad >= 6 && $edad <= 12){
                $this->globalPrioridad = $pesoEstatura + 2;
            }
            if($edad >= 13 && $edad <= 15){
                $this->globalPrioridad = $pesoEstatura + 1;
            }
        }elseif($edad >= 16 && $edad <= 40){
            $this->globalPrioridad = 2;
            if($fumador)
                $this->globalPrioridad = ($annoFumador/4) + 2;
        }elseif($edad >= 41){
            if($dieta && $edad >= 60 && $edad <= 100){
                $this->globalPrioridad = ($edad/20) + 4;
            }else{
                $this->globalPrioridad = ($edad/30) + 3;
            }
        }

        return $this->globalPrioridad;
    }

    public function riesgo($edad, $prioridad)
    {
        if($edad >= 1 && $edad <= 40){
            $this->globalRiesgo = ($edad * $prioridad) / 100;
        }
        if($this->globalEdad >= 41){
            $this->globalRiesgo = (($edad * $prioridad) / 100) + 5.3;
        }

        return $this->globalRiesgo;
    }

    public static function atencionPrioridad($edad, $prioridad)
    {
        dd($edad);
        if($prioridad > 4){
            //SE ATENDARAN LOS PACIENTES DE CUALQUIER EDADE EN URGENCIA
            $consulta = Consulta::idConsultaUrgencia()->first();
        }else{
            if($edad >= 1 && $edad <= 15){
                if($prioridad <= 4){
                    //SOLO SE ATIENDEN EN CONSULTA DE PEDIATRIA
                    $consulta = Consulta::idConsultaPediatria()->first();
                }
            }elseif($edad >=16){
                //LOS JOVENES Y ANCIANOS SE ATIENDEN EN CONSULTA DE CGI
                $consulta = Consulta::idConsultaCGI()->first();
            }
        }

        return $consulta;
    }


}
