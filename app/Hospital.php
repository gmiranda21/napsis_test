<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    //
    protected $table = 'hospitals';

    public $primaryKey = 'id_hospital';

    protected $fillable = ['nombre'];

    protected $guarded = ['id_hospital'];
}
