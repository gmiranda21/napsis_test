<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialista extends Model
{
    //
    protected $table = 'especialistas';

    public $primaryKey = 'id_especialista';

    //protected $guarded = ['id_especialista'];

    /**
     * Retorna el nombre del especialista
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNameEspecialista($query, $intTipoConsulta)
    {
        return $query->where('tipo_consulta_idtipo_consulta', $intTipoConsulta);
    }
}
