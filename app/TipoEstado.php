<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEstado extends Model
{
    //
    protected $table = 'tipo_estados';

    public $primaryKey = 'idtipo_estado';

    protected $fillable = ['nombre'];

    protected $guarded = ['idtipo_estado'];

    /**
     * Retorna Id de estado Desocupada
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIdEstadoDesocupada($query)
    {
        return $query->where('nombre', 'Desocupada');
    }
}
