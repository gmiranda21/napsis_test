<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoConsulta extends Model
{
    //
    protected $table = 'tipo_consultas';

    public $primaryKey = 'idtipo_consulta';

    //protected $fillable = ['nombre'];

    //protected $guarded = ['idtipo_consulta'];
}
