<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    //
    protected $table = 'consultas';

    public $primaryKey = 'id_consulta';

    protected $fillable = ['cantidad_pacientes', 'nombre_especialista'];

    protected $guarded = ['id_consulta'];

    /**
     * Retorna consulta estado desocupada
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEstado($query, $intTipoEstado)
    {
        return $query->where('tipo_estado_idtipo_estado', $intTipoEstado);
    }

    /**
     * Retorna id consulta tipo pediatria
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIdConsultaPediatria($query)
    {
        return $query->where('tipo_consulta_idtipo_consulta', 1)->select('id_consulta');
    }

    /**
     * Retorna id consulta tipo Urgencia
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIdConsultaUrgencia($query)
    {
        return $query->where('tipo_consulta_idtipo_consulta', 2)->select('id_consulta');
    }

    /**
     * Retorna id consulta tipo CGI
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIdConsultaCGI($query)
    {
        return $query->where('tipo_consulta_idtipo_consulta', 3)->select('id_consulta');
    }



}
